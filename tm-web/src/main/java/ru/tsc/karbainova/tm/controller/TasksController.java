package ru.tsc.karbainova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.repository.ProjectRepository;
import ru.tsc.karbainova.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.List;

@Controller
public class TasksController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/tasks")
    public ModelAndView index() {
        return new ModelAndView("task-list", "tasks", taskRepository.findAll());
    }

    @ModelAttribute("viewName")
    public String getViewName() {
        return "Tasks";
    }

    @ModelAttribute("projects")
    public Collection<Project> getProjects() {
        return projectRepository.findAll();
    }

    @ModelAttribute("task")
    public Collection<Task> getTask() {
        return taskRepository.findAll();
    }
}
