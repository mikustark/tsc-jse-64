package ru.tsc.karbainova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.karbainova.tm.dto.UserDTO;

public interface UserDtoRepository extends JpaRepository<UserDTO, String> {

    @Nullable
    UserDTO findFirstByLogin(@NotNull String login);

    void deleteByLogin(@NotNull String login);

}

