package ru.tsc.karbainova.tm.service.dto;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.repository.dto.ProjectDtoRepository;
import ru.tsc.karbainova.tm.api.service.dto.IProjectService;
import ru.tsc.karbainova.tm.dto.ProjectDTO;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService extends AbstractService<ProjectDTO> implements IProjectService, ru.tsc.karbainova.tm.api.service.dto.IOwnerService<ProjectDTO> {

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @Override
    @Transactional
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        return projectDtoRepository.findAll();
    }

    @Override
    @Transactional
    @SneakyThrows
    public void clear() {
        projectDtoRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<ProjectDTO> collection) {
        if (collection == null) return;
        for (ProjectDTO i : collection) {
            add(i);
        }
    }

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @SneakyThrows
    public ProjectDTO add(ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        projectDtoRepository.save(project);
        return project;
    }

    @Override
    @Transactional
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        projectDtoRepository.save(project);
    }


    @Override
    @Transactional
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectDtoRepository.save(project);
    }


    @Override
    @Transactional
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (project == null) throw new ProjectNotFoundException();
        projectDtoRepository.deleteByUserIdAndId(userId, project.getId());
    }

    @Override
    @Transactional
    @SneakyThrows
    public ProjectDTO updateById(@NonNull String userId, @NonNull String id,
                                 @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ProjectDTO project = findByName(userId, name);
        if (project == null) throw new EntityNotFoundException();
        project.setName(name);
        project.setDescription(description);
        projectDtoRepository.save(project);
        return project;
    }

    @Override
    @Nullable
    @SneakyThrows
    public ProjectDTO findByName(@NotNull final String userId, @NotNull final String name) {
        return projectDtoRepository.findFirstByUserIdAndName(userId, name);
    }

}
