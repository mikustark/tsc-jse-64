package ru.tsc.karbainova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.karbainova.tm.api.service.dto.ISessionService;

@NoArgsConstructor
public abstract class AbstractEndpoint {
    @NotNull
    @Autowired
    protected ISessionService sessionService;
}
