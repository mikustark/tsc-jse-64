package ru.tsc.karbainova.tm.api.entity;

public interface IHasName {
    String getName();

    void setName(String name);
}
